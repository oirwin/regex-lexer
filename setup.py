import io
import os

from setuptools import find_packages, setup

VERSION = "1.0.3"
HERE = os.path.abspath(os.path.dirname(__file__))
README = io.open(os.path.join(HERE, "README.md"), encoding="utf8").read()

setup(
    name="regex-lexer",
    description="A regular expression lexer for Pygments",
    long_description=README,
    version=VERSION,
    url="https://gitlab.inria.fr/oirwin/regex-lexer",
    author="Oliver Irwin",
    author_email="oliver.irwin@inria.fr",
    classifiers=[
        "Environment :: Plugins",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
    ],
    keywords="pygments highlight regex regexp",
    install_requires=["Pygments >= 2.14.0"],
    test_suite="tests",
    license="MIT License",
    packages=find_packages(exclude=["docs", "tests", "tests.*"]),
    entry_points="""
        [pygments.lexers]
        regex=regex:RegExpLexer
    """,
)