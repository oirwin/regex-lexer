from pygments.lexer import RegexLexer
from pygments.formatters import Terminal256Formatter, LatexFormatter
from pygments.token import *
from pygments import highlight

__all__ = ["RegExpLexer"]


class RegExpLexer(RegexLexer):
    name = "Regex"
    aliases = ["regex", "regexp"]
    filenames = []

    tokens = {
        "root": [
            (r"\s+", Text),
            (r"\w+", Text),
            (r"[<>/#!:;,§%]", Text),
            (r"\[", Keyword, "range"),
            (r"\\", Text, "escape"),
            (r"\^", Operator),
            (r"\$", Operator),
            (r"[?+*{}|.]", Keyword),
            (r"\\+", Text),
            (r"\d+", Number),
            (r"[()]", Punctuation)
        ],
        "range": [
            (r"[-^]", Keyword),
            (r"\d+", Number),
            (r"[^\]]", Text),
            (r"\]", Keyword, "#pop")
        ],
        "escape": [
            (r".", Text, "#pop")
        ]
    }
