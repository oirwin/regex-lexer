A simple regular expression lexer for [Pygments](https://pygments.org)

## Installation

To install the lexer to your `Pygments` database, use the following
command:

```bash
pip install .
```

⚠️ if your operating system warns you that Python packages should
be installed by the system-wide package manager, use the following
command:

```bash
pip install . --break-system-packages
```

